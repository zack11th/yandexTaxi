import Inputmask from "inputmask";

let phoneMask = new Inputmask("+7(999)999-9999");

window.addEventListener('load', function () {
  let $links = $('.header .nav a');
  let height = $('.header').height() + 40;

  // скролл к секциям
  $links.on('click', function(e){
    e.preventDefault();

    let id = $(this).attr('href');
    let targetPos = $(id).offset().top - height;

    if($(id).length > 0){
      $('html,body').stop(true).animate({
        scrollTop: targetPos
      }, Math.abs($(document).scrollTop() - targetPos)/2);
      $('input#mobile-menu').prop('checked', false);
    }
  });

  // подсветка соответствующего пункта nav
  $(document).on('scroll', function () {
    let scrollTop = $(this).scrollTop();

    $links.each(function () {
      let id = $(this).attr('href');
      let startPos = Math.floor($(id).offset().top - height);

      if(scrollTop >= startPos){
        $links.removeClass('active');
        $(this).addClass('active');
      }
    });
  });

  // закрытие мобильного меню при ресайзе
  $(window).on('resize', function () {
    $('input#mobile-menu').prop('checked', false);
  });


  // маска ввода телефонного намера
  let userPhone = $('.user-input[type="tel"]');
  phoneMask.mask(userPhone);

  // валидация формы
  $('form.form').on('submit', function (e) {
    e.preventDefault();
    let nameUser = $('.user-input[type="text"]').val();
    let phoneNumber = userPhone.val();
    let regexpPhone = /^\+7\([0-9]{3}\)[0-9]{3}\-[0-9]{4}$/;
    let userData = {};
    let isValidate = false;

    // валидация имени
    if (nameUser !== '') {
      userData.userName = nameUser;
    } else {
      $('.error-name').css('opacity', '1');
      setTimeout(function () {
        $('.error-name').css('opacity', '0');
      }, 3000)
    }

    // валидация телефона
    if (regexpPhone.test(phoneNumber)) {
      userData.userPhone = phoneNumber;
    } else {
      $('.error-phone').css('opacity', '1');
      setTimeout(function () {
        $('.error-phone').css('opacity', '0');
      }, 3000)
    }

    if (userData.hasOwnProperty('userName') && userData.hasOwnProperty('userPhone')) {
      $.ajax({
        url: '#',
        type: 'post',
        data: userData,
        success: function () {
          $('.user-input').val('');
          location.reload();
        }
      });
      // заглушка для демонстрации успешной отправки запроса
      $('.user-input').val('');
      console.log(`post-запрос на сервер с данными`);
      console.log(userData);
    }
  })
});