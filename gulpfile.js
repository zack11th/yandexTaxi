const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const less = require('gulp-less');
const rimraf = require('rimraf');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const post_autoprefixer = require('autoprefixer');
// const cssnano = require('cssnano');
const spritesmith = require('gulp.spritesmith');
const gulpif = require('gulp-if');
const webpackStream = require('webpack-stream');
const webpack = require('webpack');
const gcmq = require('gulp-group-css-media-queries');
const cleanCSS = require('gulp-clean-css');
const smartgrid = require('smart-grid');

let isDev = (process.argv.indexOf('--dev') !== -1);
let isProd = !isDev;

let isPug = true;

// ********************* SIMPLE TASKS *******************

//CLEAR build
gulp.task('clean', function del(cb) {
    return rimraf('build', cb);
});

// Static server
gulp.task('server', function() {
    browserSync.init({
        server: {
            port: 9000,
            baseDir: "build" //куда будет смотреть сервер
        }
    });

    gulp.watch('build/**/*').on('change', browserSync.reload);
});

// JS
let webConfig = {
    output: {
        filename: 'main.min.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: '/node_modules/'
            }
        ]

    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ],
    mode: isDev ? 'development' : 'production',
    devtool: isDev ? 'eval-source-map' : 'none'
};
gulp.task('js', function () {
    return gulp.src('src/js/main.js')
        .pipe(webpackStream(webConfig))
        .pipe(gulp.dest('build/js'));
});

// PUG or HTML
gulp.task('html', function buildHTML() {
    return gulp.src(isPug ? 'src/templates/*.pug' : 'src/templates/*.html')
        .pipe(gulpif(isPug, pug({
            pretty: true //чтобы выходной html был не в одну строчку
        })))
        .pipe(gulp.dest('build'))
});

// SASS

gulp.task('sass', function () {
    let plugins = [
         post_autoprefixer()
         // cssnano()
    ];
    return gulp.src('src/styles/main.sass')
        .pipe(gulpif(isDev, sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(gcmq())
        .pipe(postcss(plugins))
        .pipe(gulpif(isProd, cleanCSS({
            level: 2
        })))
        .pipe(rename('main.min.css'))
        .pipe(gulpif(isDev, sourcemaps.write()))
        .pipe(gulp.dest('build/css'));
});

// LESS
gulp.task('less', function () {
    let plugins = [
        post_autoprefixer()
        // cssnano()
    ];
    return gulp.src('src/styles/main.less')
        .pipe(gulpif(isDev, sourcemaps.init()))
        .pipe(less())
        .pipe(gcmq())
        .pipe(postcss(plugins))
        .pipe(gulpif(isProd, cleanCSS({
            level: 2
        })))
        .pipe(rename('main.min.css'))
        .pipe(gulpif(isDev, sourcemaps.write()))
        .pipe(gulp.dest('build/css'));
});

// SPRITES
gulp.task('sprite', function (cb) {
    let spriteData = gulp.src('src/img/icons/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../img/sprite.png',
        cssName: 'sprite.sass'
    }));
    spriteData.img.pipe(gulp.dest('build/img/'));
    spriteData.css.pipe(gulp.dest('src/styles/global/'));
    cb();
});

//COPY fonts
gulp.task('copyFonts', function () {
    return gulp.src('src/fonts/**/*.*')
        .pipe(gulp.dest('build/fonts'))
        .pipe(browserSync.stream());
});

//COPY images
gulp.task('copyImages', function () {
    return gulp.src('src/img/**/*.*')
        .pipe(gulp.dest('build/img'))
});

// COPY audio
gulp.task('copyAudio', function () {
    return gulp.src('src/audio/**/*.*')
        .pipe(gulp.dest('build/audio'))
});

// SMARTGRID
gulp.task('grid', function (done) {
    delete require.cache[require.resolve('./smartgrid.js')];

    let settings = require('./smartgrid.js');
    smartgrid('./src/styles/global', settings);

    done();
});

// *********************** CASCADE TASKS **********

// Full COPY
gulp.task('copy', gulp.parallel('copyFonts', 'copyImages', 'copyAudio'));

// Watchers
gulp.task('watch', function () {
    gulp.watch('src/templates/**/*.pug', gulp.series('html'));
    gulp.watch('src/templates/**/*.html', gulp.series('html'));
    gulp.watch('src/styles/**/*.sass', gulp.series('sass'));
    gulp.watch('src/styles/**/*.less', gulp.series('less'));
    gulp.watch('src/js/**/*.js', gulp.series('js'));
    gulp.watch('src/templates/blocks/**/*.js', gulp.series('js'));
    gulp.watch('src/templates/blocks/**/*.sass', gulp.series('sass'));
    gulp.watch('src/templates/blocks/**/*.less', gulp.series('less'));
    // gulp.watch('./smartgrid.js', gulp.series('grid'));
});

// ****************** WORK TASKS ***************************

//BUILD
gulp.task('build', gulp.series(
    // 'grid',
    'clean',
    gulp.parallel('html', 'sass', 'js', 'sprite', 'copy')
    )
);

gulp.task('build_less', gulp.series(
    'grid',
    'clean',
    gulp.parallel('html', 'less', 'js', 'sprite', 'copy')
    )
);

//DEVELOP
gulp.task('default', gulp.series(
    'build',
    gulp.parallel('watch', 'server')
    )
);

gulp.task('dev_less', gulp.series(
    'build_less',
    gulp.parallel('watch', 'server')
    )
);